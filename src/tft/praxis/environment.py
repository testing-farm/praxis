import click

from . import Configuration, prettify_json, SessionEnvironmentDimension

from typing import cast, Callable, List, Optional


class EnvironmentDimension:
    name: str
    label: str
    command_name: str
    metavar: str

    def __init__(
        self,
        cfg: Configuration,
    ) -> None:
        self.cfg = cfg

    @property
    def known_entries(self) -> List[str]:
        assert self.cfg.raw_config

        return cast(List[str], self.cfg.raw_config['environment-dimensions'][self.name]['known-entries'])

    @property
    def known_entries_label(self) -> str:
        return ', '.join([f'"{name}"' for name in self.known_entries])

    @classmethod
    def create_root_command(cls, parent: click.Group) -> Callable[..., None]:
        @parent.command(name=cls.command_name, short_help=f'Pick {cls.label} to use.')
        @click.argument('entry_name', metavar=f'[{cls.metavar}]', nargs=-1)
        @click.pass_obj
        def cmd(cfg: Configuration, entry_name: Optional[str]) -> None:
            assert cfg.session is not None
            assert cfg.logger is not None

            dimension = cls(cfg)

            if dimension.name in cfg.session.environment_dimensions:
                session_container = cfg.session.environment_dimensions[dimension.name]

            else:
                session_container = cfg.session.environment_dimensions[dimension.name] = SessionEnvironmentDimension()

            if not entry_name:
                current_picked = session_container.current

                if not current_picked:
                    cfg.logger.error(f'{cls.label} not set - possible choices are {dimension.known_entries_label}')

                cfg.logger.info(f'current {cls.name} is "{current_picked}"')

                if cfg.output_format != 'human':
                    click.echo(prettify_json({cls.name: current_picked}))

                return

            new_picked = entry_name[0]

            if new_picked not in dimension.known_entries:
                cfg.logger.error(
                    f'Unknown {cls.label} "{new_picked}" - possible choices are {dimension.known_entries_label}'
                )

            session_container.current = new_picked

            cfg.session.save(cfg)

            cfg.logger.info(f'switching {cls.name} to "{new_picked}"')

            if cfg.output_format != 'human':
                click.echo(prettify_json({cls.name: new_picked}))

        return cmd
