= Praxis

Praxis is a small utility library, providing common functionality needed by CLI apps developed for TFT projects. It builds on https://palletsprojects.com/p/click/[Click] and https://rich.readthedocs.io/en/latest/[Rich] libraries.

The tools built with this library share the following features:

* they deal with multiple "environments", or "deployments". For example, a tool single installation of `cool-service-cli` tool could be used to manage several deployments of `cool-service` service - production, staging, development, etc. - easily switching between them.
* they need complex command groups. With Click, it is easy to add and manage commands and groups, Praxis adds easy-to-use command discovery.
* they are used by both humans and machines. Nicely readable output for humans, while supporting machine-readable output is a must.

Besides that, Praxis adds a few more bells and whistles:

* configuration and session handling
* running commands, fetching remote URLs
* progress bars, prompts, ...


== Installation

[source,shell]
....
$ pip install tft-praxis
....

== Usage

Praxis provides ready-to-use core commands one can start build upon. To extend them, it uses https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins[entry points] for discovery.

Let's build a small utility of our own, in a simple `cool_cli.py` file.

.cool_cli.py
[source,python]
....
import click

from tft.praxis import Configuration
from tft.praxis.root import create_cli_root

from typing import List


@click.group(name='math', short_help='Math operations.')
@click.pass_obj
def cmd_math(cfg: Configuration) -> None:
    pass


@cmd_math.command(name='add', short_help='Add two arguments.')
@click.argument('numbers', metavar='N...', nargs=2)
@click.pass_obj
def cmd_math_add(cfg: Configuration, numbers: List[str]) -> None:
    print(sum([float(number) for number in numbers]))


cli_root = create_cli_root(entry_points=['cool_cli.command_plugins'])
....

Our CLI will provide one command group, `cool-cli math`, which will have a single command, `cool-cli math add`. We start by creating a group and its command, using well-known https://click.palletsprojects.com/en/7.x/commands/[Click concepts] like `@click.group` and `@click.argument`.

[NOTE]
====
All our commands have access - via `@click.pass_obj` decorator - to a configuration managed by Praxis core. The configuration is loaded from a file when CLI tool starts, if available.
====

The last line of our `cool_cli.py` hands asks Praxis to create a root command, a Click command we can then use as a starting point of the `cool-cli` script.

Next we need to connect the dots, so to say. In the package description of your choice - `setup.py`, `pyproject.toml`, etc. - we need to inform packaging that there we would like to use an entry point called `cool_cli.command_plugins`, and what objects are attached to it:

.pyproject.toml
[source,toml]
....
[tool.poetry.plugins."cool_cli.command_plugins"]
"math" = "cool_cli:cmd_math"
....

This let's Praxis core to discover our cool CLI's commands: it scans `cool_cli.command_plugins`, as instructed on the last line of our `cool_cli.py`, and Python packaging tools take care of making sure our `cmd_math` function is visible under this entry point.

We should also create a `cool-cli` script. Once again, using entry points:

.pyproject.toml
[source,toml]
....
[tool.poetry.scripts]
cool-cli = "cool_cli:cli_root"
....

Note that we did not write any `cli_root` command. Instead, we asked Praxis to create it for us via `create_cli_root`, telling it to scan the entry point with our commands.

After we install our cool CLI package, we can profit from Praxis goodies:

[source,shell]
....
$ cool-cli
Usage: cool-cli [OPTIONS] COMMAND [ARGS]...

Options:
  --config TEXT          Path to the configuration directory
  --format [human|json]  Print command output in human or machine format.
  --help                 Show this message and exit.

Commands:
  completion  Shell completion commands.
  init        Initialize configuration file.
  math        Math operations.
....

There is our command group, `math`, with a few commands provided by Praxis:

* `init` can be run to create configuration file for our CLI tool. Out-of-the-box, the config file is pretty much empty, as Praxis developers do not know what our tool needs.
* `completion` generates Bash completion scripts.

Let's run our command:

[source,shell]
....
$ cool-cli math
Config file ~/.config/cool-cli/config.yaml does not exists, running configuration wizard
Do you wish to check the configuration file, possibly modifying it? [Y/n]:  
Do you wish to save this as your configuration file? [y/N]: y
Saved, your config file has been updated
....

When executed for the first time, `cool-cli` notices there is no configuration file, and runs a wizard to create it. Because we did not bother to implement out own wizard, a default one is used - no questions asked. The final config file is very simple:

.~/.config/cool-cli/config.yaml
[source,yaml]
....
environment-dimensions: {}
....

[NOTE]
====
One might think why would even such a simple CLI tool like our `cool-cli` need a config file. It is true, for our example this feature is, indeed, an overkill. Please, be aware Praxis aims at more complex tools that actually deal with multiple environments, and this feature is needed in every CLI tool we developed, therefore it is not an optional feature. This may change in the future, though.
====

Now we have a config file, we can try again:

[source,shell]
....
$ cool-cli math
Usage: cool-cli math [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  add  Add two arguments.
....

[source,shell]
....
$ cool-cli math add 1 2
3.0
....
