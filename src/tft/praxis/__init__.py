import concurrent.futures
import dataclasses
import json
import logging
import pkg_resources
import re
import time
import shlex
import subprocess
import sys

import click
import click_spinner
import click_completion
import jsonschema
import requests
import ruamel.yaml
import ruamel.yaml.compat
import stackprinter
import urlnormalizer

from typing import cast, Any, Callable, Dict, Iterable, List, NamedTuple, NoReturn, Optional, Tuple, TypeVar
from rich.console import Console
from rich.table import Table
from rich.markup import escape
from rich.style import Style
from rich import box


# Simple url regex used to detect links in tables
REGEX_URL = r'(?i)http(s)?:\/\/(www\.)?[^\s()\[\]<>]+'


stackprinter.set_excepthook(
    style='darkbg2', source_lines=7, show_signature=True, show_vals='all', reverse=False, add_summary=False
)

click_completion.init()


class ValidationResult(NamedTuple):
    result: bool
    errors: List[Any]


class ColorFormatter(logging.Formatter):
    """
    Modified logging.Formatter class to support message coloring on different log levels.
    """

    def __init__(self, fmt: str = '%(msg)s'):
        self.fmt = fmt
        self.COLORS = {
            logging.DEBUG: Style(color='white'),
            logging.INFO: Style(color='green'),
            logging.WARNING: Style(color='yellow'),
            logging.ERROR: Style(color='red'),
            'DEFAULT': Style(color='white'),
        }
        super().__init__(self.fmt)

    def format(self, record: logging.LogRecord) -> Any:
        colorizer = self.COLORS.get(record.levelno) or self.COLORS['DEFAULT']
        return colorizer.render(logging.Formatter.format(self, record))  # noqa


class Logger:
    """
    Simple class providing semantic logging.
    """

    def __init__(self, context: Optional[str] = None) -> None:

        # Using 'default' name for loggers without context. Not using any name would return
        # the root logger - we don't want to modify it as it also affects all its children.
        self.logger = logging.getLogger(context or 'default')
        self.logger.level = logging.DEBUG

        if not self.logger.hasHandlers():
            hdlr = logging.StreamHandler()
            self.logger.addHandler(hdlr)

        for handler in self.logger.handlers:
            fmt = f'[{context}] %(msg)s' if context else '%(msg)s'
            formatter = ColorFormatter(fmt=fmt)
            handler.setFormatter(formatter)

    def debug(self, msg: str) -> None:
        # We need to introduce options controling logging output first!
        # click.echo()
        pass

    def info(self, msg: str) -> None:
        self.logger.info(msg)

    def warn(self, msg: str) -> None:
        self.logger.warning(msg)

    def error(self, msg: str) -> NoReturn:
        self.logger.error(msg)

        sys.exit(1)


@dataclasses.dataclass
class SessionEnvironmentDimension:
    current: Optional[str] = dataclasses.field(default=None)

    @classmethod
    def from_yaml(cls, data: Dict[str, Any]) -> 'SessionEnvironmentDimension':
        return cls(**data)


@dataclasses.dataclass
class Session:
    environment_dimensions: Dict[str, SessionEnvironmentDimension] = dataclasses.field(default_factory=dict)

    @classmethod
    def from_yaml(cls, data: Dict[str, Any]) -> 'Session':
        return Session(
            environment_dimensions={
                dimension_name: SessionEnvironmentDimension.from_yaml(dimension_data)
                for dimension_name, dimension_data in data['environment-dimensions'].items()
            }
        )

    @classmethod
    def initialize(cls, cfg: 'Configuration') -> None:
        cfg.session = cls()
        cfg.session.save(cfg)

    def save(self, cfg: 'Configuration') -> None:
        assert cfg.session_filepath is not None

        save_yaml(dataclasses.asdict(self), cfg.session_filepath)


@dataclasses.dataclass
class Configuration:
    raw_config: Optional[Any] = None

    logger: Optional[Logger] = None

    config_dirpath: Optional[str] = None
    config_filepath: Optional[str] = None
    session_filepath: Optional[str] = None

    session: Optional[Session] = None

    completion_shell: Optional[str] = None

    output_format: str = 'human'

    env_config: Optional[Dict[str, Any]] = None

    def get_current_environment(self, dimension: str) -> Optional[str]:
        assert self.session

        if dimension not in self.session.environment_dimensions:
            return None

        return self.session.environment_dimensions[dimension].current

    def has_current_environment(self, dimension: str) -> bool:
        assert self.raw_config

        picked_name = self.get_current_environment(dimension)

        if not picked_name:
            return False

        return picked_name in self.raw_config['environment-dimensions'][dimension]

    def get_current_environment_config(self, dimension: str) -> Any:
        assert self.raw_config

        return self.raw_config['environment-dimensions'][dimension][self.get_current_environment(dimension)]


# Colorization
def BLUE(s: str) -> str:
    return f'[blue]{s}[/blue]'


def CYAN(s: str) -> str:
    return f'[cyan]{s}[/cyan]'


def GREEN(s: str) -> str:
    return f'[green]{s}[/green]'


def RED(s: str) -> str:
    return f'[red]{s}[/red]'


def YELLOW(s: str) -> str:
    return f'[yellow]{s}[/yellow]'


def WHITE(s: str) -> str:
    return f'[white]{s}[/white]'


def NL() -> None:
    click.echo('', file=sys.stderr)


def load_yaml(filepath: str) -> Any:
    with open(filepath, 'r') as f:
        return ruamel.yaml.safe_load(f)


def save_yaml(data: Any, filepath: str) -> None:
    with open(filepath, 'w') as f:
        ruamel.yaml.dump(data, f)

        f.flush()


def validate_struct(
    logger: Logger,
    data: Any,
    package_name: str,
    schema_name: str
) -> ValidationResult:
    try:
        schema_filepath = pkg_resources.resource_filename(package_name, f'schemas/{schema_name}.yaml')

    except ModuleNotFoundError:
        logger.error(f'Cannot find schema "{schema_name}" in package "{package_name}"')

    schema = load_yaml(schema_filepath)

    try:
        jsonschema.validate(instance=data, schema=schema)

        return ValidationResult(True, [])

    except jsonschema.exceptions.ValidationError:
        validator = jsonschema.Draft4Validator(schema)

        return ValidationResult(False, validator.iter_errors(data))


def prettify_json(data: Any, flag: bool = True) -> str:
    if not flag:
        return json.dumps(data)

    return json.dumps(data, sort_keys=True, indent=4)


def prettify_yaml(data: Any, flag: bool = True) -> str:
    Y = ruamel.yaml.YAML()

    if flag:
        Y.indent(sequence=2, mapping=2, offset=0)

    stream = ruamel.yaml.compat.StringIO()

    Y.dump(data, stream)

    return stream.getvalue()


def execute_command(
    cmd: List[str],
    spinner: bool = False,
    logger: Optional[Logger] = None,
    accept_exit_codes: Optional[Iterable[int]] = None,
    **kwargs: Any
):
    # type: (...) -> subprocess.CompletedProcess[bytes]

    accept_exit_codes = accept_exit_codes or [0]

    logger = logger or Logger()

    with get_spinner(disable=not spinner):
        try:
            result = subprocess.run(cmd, **kwargs)

        except subprocess.SubprocessError as exc:
            logger.error(f'Failed to complete command: {exc}')

    if result.returncode not in accept_exit_codes:
        stdout = result.stdout.decode('utf-8') if result.stdout else ''
        stderr = result.stderr.decode('utf-8') if result.stderr else ''

        logger.error(
            f"""
Failed to complete command, exited with code {result.returncode}:

{shlex.quote(' '.join(cmd))}

STDOUT: ---v---v---v---v---v---
{stdout}
        ---^---^---^---^---^---

STDERR: ---v---v---v---v---v---
{stderr}
        ---^---^---^---^---^---
""")

    return result


def fetch_remote(
    url: str,
    logger: Optional[Logger] = None,
    spinner: bool = False,
    method: str = 'get',
    request_kwargs: Optional[Dict[str, Any]] = None,
    on_error: Optional[Callable[[requests.Response], None]] = None,
    accepted_codes: List[int] = [200],
) -> requests.Response:
    logger = logger or Logger()
    request_kwargs = request_kwargs or {}
    url = urlnormalizer.normalize_url(url)

    with get_spinner(disable=not spinner):
        if method == 'get':
            res = requests.get(url, **request_kwargs)

        elif method == 'post':
            res = requests.post(url, **request_kwargs)

        elif method == 'delete':
            res = requests.delete(url, **request_kwargs)

    if res.status_code not in accepted_codes:
        if on_error:
            on_error(res)

        else:
            logger.error(f'Failed to communicate with remote url {url}, responded with code {res.status_code}')

    return res


def print_table(cfg: Configuration, table: List[List[str]], format: Optional[str] = None) -> None:

    console = Console()

    if not format:
        format = 'json' if cfg.output_format != 'human' else 'text'

    def _to_items() -> List[Dict[str, str]]:
        as_list = []

        headers = table[0]

        for row in table[1:]:
            as_list.append({header.lower().replace(' ', '_'): cell for header, cell in zip(headers, row)})

        return as_list

    def _replace_link(link: Any) -> str:
        return f'[link={link.group()}]LINK[/link] {link.group()}'

    if format == 'text':
        if len(table) > 1:
            rich_table = Table(box=box.HEAVY_HEAD)

            for header in table[0]:
                rich_table.add_column(header)

            for row in table[1:]:
                rich_row = []
                for cell in row:
                    rich_cell = cell if isinstance(cell, str) else rich_escape_json_list(prettify_json(cell))

                    # Add clickable 'LINK' button next to every url in case the link gets broken into multiple lines
                    rich_cell = re.sub(REGEX_URL, _replace_link, rich_cell)
                    rich_row.append(rich_cell)
                rich_table.add_row(*rich_row)

            console.print(rich_table)

    # Before printing in JSON or YAML format, we need to get rid of every markup tag inside strings. This is done
    # using Console.render_str creates a Text object which correctly parses the tags, .plain property returns the
    # original string without any formatting.
    elif format == 'json':
        printable = rich_escape_json_list(prettify_json(_to_items()))
        click.echo(console.render_str(printable).plain)

    elif format == 'yaml':
        printable = prettify_yaml(_to_items())
        click.echo(console.render_str(printable).plain)

    else:
        assert False, 'Table format {} is not supported'


JobReturnType = TypeVar('JobReturnType')
JobCallbackType = Callable[..., JobReturnType]
JobType = Tuple[JobCallbackType[JobReturnType], List[Any], Dict[str, Any]]


def execute_jobs(jobs: List[JobType[JobReturnType]], max_workers: Optional[int] = None):
    # type: (...) -> List[concurrent.futures.Future[JobReturnType]]

    futures = []

    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        for callback, args, kwargs in jobs:
            futures.append(executor.submit(callback, *args, **kwargs))

        done, pending = concurrent.futures.wait(futures)

        assert not pending

    return futures


def get_progressbar(iterable: Iterable[Any]) -> Any:
    """Wrapped click.progressbar function to print to stderr."""
    return click.progressbar(iterable, file=sys.stderr)


def get_spinner(disable: bool) -> Any:
    """Wrapped click_spinner.spinner function to print to stderr."""
    return click_spinner.spinner(disable=disable, stream=sys.stderr)


def confirm(cfg: Configuration, msg: str, force: bool, default: bool = False, abort: bool = False,) -> bool:
    """Wrapped click.confirm function to print to stderr."""
    assert cfg.logger is not None

    if force:
        return force

    if cfg.output_format == 'human':
        console = Console(file=sys.stderr)
        console.print(msg, end='')
        return click.confirm('', default=default, abort=abort, err=True)

    if abort:
        cfg.logger.info(msg)
        raise click.Abort()

    return False


def prompt(cfg: Configuration, msg: str, type: Any = None, default: Optional[str] = None) -> str:
    """Wrapped click.prompt function to print to stderr."""
    assert cfg.logger is not None

    if cfg.output_format == 'human':
        console = Console(file=sys.stderr)
        console.print(msg, end='')
        return cast(str, click.prompt('', type=type, default=default, err=True))

    cfg.logger.error('click.prompt() unsupported in non-human output mode, please use the command-line options instead')


def rich_escape_json_list(string: str) -> str:
    """
    This function replaces square brackets representing JSON lists of `json.dumps()` output.
    Rich markup tags (e.g [red]text[/red]) stored in JSON strings will be preserved.
    """

    # Split the string at every '"' into list, every odd index of this list is outside JSON string,
    # every square bracket in these places every square bracket will be doubled (escaped).
    string_list = string.split('"')
    for i in range(0, len(string_list), 2):
        string_list[i] = escape(string_list[i])

    return '"'.join(string_list)


def wait(label: str, check: Callable[[], None], timeout: int, tick: int, logger: Optional[Logger] = None) -> None:
    """
    Wait for a condition to be true.
    """

    logger = logger or Logger()
    logger.info(f'{label}, timeout in {timeout / 60} minutes')
    end = time.time() + timeout
    while time.time() < end:
        if check():
            break
        time.sleep(tick)
    else:
        logger.error(f'Timed out while {label}')


def guess_app_name() -> str:
    return sys.argv[0]


def guess_config_dirpath() -> str:
    return click.get_app_dir(guess_app_name())


def from_plugins(*entry_points: str) -> Callable[[click.Group], click.Group]:
    def _from_plugins(group_command: click.Group) -> click.Group:
        if not isinstance(group_command, click.Group):
            raise TypeError("Plugins can only be attached to an instance of click.Group()")

        for entry_point in entry_points:
            for cmd_point in pkg_resources.iter_entry_points(entry_point):
                group_command.add_command(cmd_point.load())

        return group_command

    return _from_plugins
