import os
import shutil
import tempfile
import sys

import click

from . import Configuration, GREEN, RED, YELLOW, WHITE, confirm, prompt, prettify_yaml

from typing import Any, Dict
from rich.console import Console


class ConfigInitializer:
    def __init__(self, cfg: Configuration) -> None:
        self.cfg = cfg
        self.console = Console(file=sys.stderr)

        self.config: Dict[str, Any] = {}

    def NL(self) -> None:
        self.console.print('')

    def TITLE(self, s: str) -> None:
        self.console.print(YELLOW(s))

    def TEXT(self, s: str) -> None:
        self.console.print(WHITE(s))

    def ERROR(self, s: str) -> None:
        self.console.print(RED(s))

    def WARN(self, s: str) -> None:
        self.console.print(GREEN(s))

    def SUCCESS(self, s: str) -> None:
        self.console.print(GREEN(s))

    def QUESTION(self, s: str) -> str:
        return YELLOW(s)

    def prompt(self, question: str, *args: Any, **kwargs: Any) -> str:
        return prompt(
            self.cfg,
            self.QUESTION(question),
            *args,
            **kwargs
        )

    def confirm(self, question: str, *args: Any, **kwargs: Any) -> bool:
        return confirm(
            self.cfg,
            self.QUESTION(question),
            *args,
            **kwargs
        )

    def init_config(self) -> None:
        self.config['environment-dimensions'] = {}

    def ask(self) -> None:
        pass

    def save_config(self) -> None:
        tmp_config_file = tempfile.NamedTemporaryFile(mode='w', delete=False)

        with tmp_config_file:
            tmp_config_file.write(prettify_yaml(self.config))

        if self.confirm('Do you wish to check the configuration file, possibly modifying it?', False, default=True):
            click.edit(filename=tmp_config_file.name)

        if self.confirm('Do you wish to save this as your configuration file?', False, default=False):
            assert self.cfg.config_dirpath is not None
            assert self.cfg.config_filepath is not None

            os.makedirs(self.cfg.config_dirpath, exist_ok=True)

            if os.path.exists(self.cfg.config_filepath):
                os.unlink(self.cfg.config_filepath)

            shutil.copy(tmp_config_file.name, self.cfg.config_filepath)

            self.SUCCESS('Saved, your config file has been updated')

        else:
            self.WARN('Ok, your answers were thrown away.')

    def run(self) -> None:
        self.init_config()
        self.ask()
        self.save_config()


@click.command(name='init', short_help='Initialize configuration file.')
@click.pass_obj
def cmd_init(cfg: Configuration) -> None:
    """
    Using a series of questions, initialize a configuration file. Some information can be extracted automatically,
    other bits must be explicitly provided by the user.
    """

    cfg_initializer = ConfigInitializer(cfg)

    cfg_initializer.run()
