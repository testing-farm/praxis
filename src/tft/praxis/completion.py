import click
import click_completion
import click_completion.core

from . import Configuration

from typing import Optional


@click.group(name='completion', short_help='Shell completion commands.')
@click.pass_obj
@click.option('--shell', required=False, type=click_completion.DocumentedChoice(click_completion.core.shells))
def cmd_completion(cfg: Configuration, shell: Optional[str] = None) -> None:
    """
    Command completion for your shell.
    """

    cfg.completion_shell = shell


@cmd_completion.command(name='show')
@click.pass_obj
def cmd_show(cfg: Configuration) -> None:
    """
    Show the command completion code.
    """

    click.echo(click_completion.core.get_code(cfg.completion_shell))


@cmd_completion.command(name='install')
@click.pass_obj
@click.option('--append / --overwrite', help='Append the completion code to the file, or overwrite it.', default=False)
@click.option('--path', help='Path to a completion file.', default='~/.bash_completion')
def cmd_install(cfg: Configuration, append: bool = False, path: Optional[str] = None) -> None:
    """
    Install the command completion code.
    """

    assert cfg.logger is not None

    shell, path = click_completion.core.install(shell=cfg.completion_shell, path=path, append=append)

    cfg.logger.info(f'{shell} completion installed in {path}')
