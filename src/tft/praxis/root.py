import os.path
import sys

import click

from . import Logger, Configuration, Session, NL, load_yaml, validate_struct, guess_config_dirpath, from_plugins
from .init import cmd_init

from typing import cast, Any, Callable, List, Optional


def create_cli_root(
    package_name: str = 'tft.praxis',
    entry_points: Optional[List[str]] = None
) -> Callable[..., None]:
    entry_points = ['tft_praxis.command_plugins'] + (entry_points or [])

    @from_plugins(*entry_points)
    @click.group()
    @click.pass_context
    @click.option(
        '--config', default=guess_config_dirpath,
        help='Path to a configuration file.'
    )
    @click.option(
        '--format', type=click.Choice(['human', 'json']),
        help='Print command output in human or machine format.'
    )
    def cli_root(ctx: Any, config: str, format: Optional[str]) -> None:
        ctx.ensure_object(Configuration)

        cfg = cast(Configuration, ctx.obj)

        cfg.logger = Logger()

        cfg.config_dirpath = os.path.expanduser(config)
        cfg.config_filepath = os.path.join(cfg.config_dirpath, 'config.yaml')
        cfg.session_filepath = os.path.join(cfg.config_dirpath, 'session.yaml')

        cfg.output_format = format if format else cfg.output_format

        logger = Logger()

        if not os.path.exists(cfg.config_dirpath) or not os.path.exists(cfg.config_filepath):
            if ctx.invoked_subcommand != 'init':
                logger.warn(f'Config file {cfg.config_filepath} does not exists, running configuration wizard')

            ctx.invoke(cmd_init)
            sys.exit(0)

        cfg.raw_config = load_yaml(cfg.config_filepath)

        validation = validate_struct(cfg.logger, cfg.raw_config, package_name, 'config')

        if not validation.result:
            logger.warn(f'Config file {cfg.config_filepath} must be updated, found following validation errors:')

            for error in validation.errors:
                NL()
                cfg.logger.warn(f'  * {error.message}')
                NL()

            logger.info('Running configuration wizard')

            ctx.invoke(cmd_init)
            sys.exit(0)

        if os.path.exists(cfg.session_filepath):
            data = load_yaml(cfg.session_filepath)

            if not validate_struct(cfg.logger, data, package_name, 'session').result:
                logger.warn('Session file is corrupted and will be replaced with new session.',)

                Session.initialize(cfg)

            else:
                cfg.session = Session.from_yaml(data)

        else:
            cfg.logger.warn('session does not exist yet, initializing')

            Session.initialize(cfg)

        assert cfg.session

#        Following code makes sure that when given commands are executed, they run with a given environment
#        dimension selected and known. This is based on downstream code (env), and it must become generalized
#        so CLI tools can hook into this mechanism.
#
#        current_env_name = cfg.get_current_environment('env')
#
#        if current_env_name:
#            if not cfg.has_current_environment('env'):
#                cfg.logger.error(f'Environment {current_env_name} is not configured')
#
#            cfg.env_config = cfg.get_current_environment_config('env')
#            cfg.logger = Logger(current_env_name)
#
#        elif ctx.invoked_subcommand not in ('completion', 'env', 'init'):
#            cfg.logger.error(
#                f'Specify environment via `{guess_app_name()} env`'
#            )

    return cli_root


cli_root = create_cli_root()
